layout (points) in;
layout (triangles, max_vertices = 12) out;

in VS_OUT {
    unsigned norm;
    unsigned tex_pos;
} gs_in[];

out vec3 f_norm;
out vec3 f_tex_pos;

void main() {
    if(norm == 0) {
        f_norm      = vec3(1., 0., 0.);
        f_tex_pos   = vec3(0., 0., 0,.);
        gl_Position = gl_in[0].gl_Position + vec4(1., 0., 0., 0.);
        EmitVertex();
        f_norm      = vec3(1., 0., 0.);
        f_tex_pos   = vec3(0., 0., 0,.);
        gl_Position = gl_in[0].gl_Position + vec4(1., 0., 0., 0.);
        EmitVertex();
    }
        gl_Position = gs_in[i].line_end;
        EmitVertex();
        EndPrimitive();
    }
}
