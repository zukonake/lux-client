layout (location = 0) in vec3 pos;

struct Light {
    vec3 pos;
    vec3 col;
    vec3 rad;
};

uniform Light light;
uniform mat4  transform;

void main() {
    gl_Position = transform * vec4(light.pos + pos * light.rad, 1.0);
}
