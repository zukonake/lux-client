layout (location = 0) in vec3 pos;
layout (location = 1) in vec3 norm;

out vec3 f_map_pos;
out vec3 f_norm;

uniform mat4 transform;
uniform vec3 map_pos;

void main()
{
    gl_Position = transform * vec4(pos, 1.0);
    f_norm = norm;
    f_map_pos = map_pos + pos;
}
