#pragma once

#include <gl.hpp>
//
#include <GLFW/glfw3.h>
//
#include <lux_shared/common.hpp>

namespace ui {

struct MouseEvent {
    Vec2F pos;
    int   button;
    int   action;
};

struct ScrollEvent {
    Vec2F pos;
    F64   off;
};

struct KeyEvent {
    int key;
    int action;
};

struct IoContext {
    DynArr<MouseEvent>  mouse_events;
    DynArr<ScrollEvent> scroll_events;
    DynArr<KeyEvent>    key_events;
    Vec2F               mouse_pos;
    GLFWwindow         *win;
};

using Transform = glm::mat4;
using NodeId = U16;

struct Node;

struct Handle {
    NodeId id;
    bool is_valid() const;
    Node& operator*() const;
    Node* operator->() const;

    void erase();
    Handle() = default;
};

void default_io_tick(Handle, Transform const&, IoContext&);

//@TODO consider moving to .cpp
struct Node {
    DynArr<Handle> children;
    //@TODO event loop and united functions
    void (*deinit)(U32) = nullptr;
    void (*io_tick)(Handle, Transform const&, IoContext&) = &default_io_tick;
    Transform tr = glm::mat4(1.f);
    bool      fixed_aspect = false;
    U8        priority = 0;
};

extern Handle screen;

Handle create_node(Handle parent, U8 priority = 0);

void children_tick(Handle h, Transform const&, IoContext&);
void window_sz_cb(Vec2U const& old_window_sz, Vec2U const& window_sz);
void init();
void deinit();
void tick();
void send_mouse_event(MouseEvent const& event);
void send_scroll_event(ScrollEvent const& event);
void send_key_event(KeyEvent const& event);

}
