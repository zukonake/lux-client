#include <cstring>
//
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/fast_square_root.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtx/quaternion.hpp>
//
#include <lux_shared/common.hpp>
#include <lux_shared/map.hpp>
#include <lux_shared/util/packer.hpp>
//
#include <imgui/imgui.h>
#include <gl.hpp>
#include <db.hpp>
#include <client.hpp>
#include <ui.hpp>
#include <deferred_renderer.hpp>
#include <directional_lightning.hpp>
#include <point_lightning.hpp>
#include <entity.hpp>
#include "map.hpp"

static gl::VertFmt vert_fmt;
static GLuint      program;
static GLuint      tileset;
static VecMap<ChkPos, DynArr<EntityId>> entities_chunk_map;

struct Mesh {
#pragma pack(push, 1)
    struct Vert {
        Vec3<U8> pos;
        U8       norm;
        U8       tex;
    };
#pragma pack(pop)

    gl::VertBuff v_buff;
    gl::IdxBuff i_buff;
    gl::VertContext context;
    DynArr<Vert> verts;
    DynArr<U16>  idxs;
    bool is_allocated;

    void alloc() {
        LUX_ASSERT(not is_allocated);
        v_buff.init();
        i_buff.init();
        context.init({v_buff}, vert_fmt);
        is_allocated = true;
    }

    void dealloc_and_unload(ChkPos const& pos) {
        cs_sgnl.chunk_mesh_unload.chunks.insert(pos);
        dealloc();
    }

    void dealloc() {
        LUX_ASSERT(is_allocated);
        context.deinit();
        i_buff.deinit();
        v_buff.deinit();
        is_allocated = false;
    }

    void operator=(Mesh&& that) {
        v_buff  = move(that.v_buff);
        i_buff  = move(that.i_buff);
        context = move(that.context);
        verts   = move(that.verts);
        idxs    = move(that.idxs);
        is_allocated = move(that.is_allocated);
        that.is_allocated = false;
    }
    Mesh() : is_allocated(false) { }
    Mesh(Mesh&& that) { *this = move(that); }
};

static Mesh debug_mesh_0;
static Mesh debug_mesh_1;

static DynArr<Mesh>   meshes;
VecSet<ChkPos>        chunk_mesh_requests;

static ChkCoord render_dist = 0;
static ChkPos   last_player_chk_pos = to_chk_pos(glm::floor(last_player_pos));

void map_init() {
    char const* tileset_path = "tileset.png";
    Vec2U const block_size = {16, 16};
    program  = load_program("glsl/block.vert" , "glsl/block.frag");
    Vec2U tileset_size;
    tileset = load_texture(tileset_path, tileset_size);
    Vec2F tex_scale = (Vec2F)block_size / (Vec2F)tileset_size;

    glUseProgram(program);
    set_uniform("tex_scale", program, glUniform2fv,
                1, glm::value_ptr(tex_scale));
    vert_fmt.init(
        {{3, GL_UNSIGNED_BYTE, false, false},
         {1, GL_UNSIGNED_BYTE, false, false},   //@TODO this should be unsigned
         {1, GL_UNSIGNED_BYTE, false, false}}); //~this as well

    gl::VertContext::unbind_all();
    debug_mesh_0.alloc();

    debug_mesh_0.verts.resize(4);
    debug_mesh_0.idxs.resize(6);
    for(Uns i = 0; i < 6; ++i) {
        debug_mesh_0.idxs[i] = quad_idxs<U16>[i];
    }
    for(Uns i = 0; i < 4; ++i) {
        debug_mesh_0.verts[i].pos  = Vec3<U8>(u_quad<U32>[i] * (U32)CHK_SIZE, 0);
        debug_mesh_0.verts[i].norm = 0b101;
        debug_mesh_0.verts[i].tex  = 0;
    }

    debug_mesh_0.i_buff.bind();
    debug_mesh_0.i_buff.write(6, debug_mesh_0.idxs.beg, GL_STATIC_DRAW);
    debug_mesh_0.v_buff.bind();
    debug_mesh_0.v_buff.write(4, debug_mesh_0.verts.beg, GL_STATIC_DRAW);

    gl::VertContext::unbind_all();
    debug_mesh_1.alloc();

    debug_mesh_1.verts.resize(4);
    debug_mesh_1.idxs.resize(6);
    for(Uns i = 0; i < 6; ++i) {
        debug_mesh_1.idxs[i] = quad_idxs<U16>[i];
    }
    for(Uns i = 0; i < 4; ++i) {
        debug_mesh_1.verts[i].pos  = Vec3<U8>(u_quad<U32>[i] * (U32)(CHK_SIZE / 4), 0);
        debug_mesh_1.verts[i].norm = 0b101;
        debug_mesh_1.verts[i].tex  = 1;
    }

    gl::VertContext::unbind_all();
    debug_mesh_1.i_buff.bind();
    debug_mesh_1.i_buff.write(6, debug_mesh_1.idxs.beg, GL_STATIC_DRAW);
    debug_mesh_1.v_buff.bind();
    debug_mesh_1.v_buff.write(4, debug_mesh_1.verts.beg, GL_STATIC_DRAW);
    meshes.resize(pow(2 * render_dist + 1, 3));

    directional_lightning::init();
    //point_lightning::init();
}

void map_deinit() {
    directional_lightning::deinit();
    //point_lightning::deinit();

    for(auto& mesh : meshes) {
        if(mesh.is_allocated) {
            mesh.dealloc();
        }
    }
    meshes.dealloc_all();
    debug_mesh_0.dealloc();
}

void map_io_tick(ui::Handle, ui::Transform const& tr, ui::IoContext& context) {
}

///returns -1 if out of bounds
static Int get_fov_idx(ChkPos const& pos) {
    ChkPos idx_pos = (pos - last_player_chk_pos) + render_dist;
    ChkCoord size = render_dist * 2 + 1;
    if(clamp(idx_pos, ChkPos(0), ChkPos(size - 1)) != idx_pos) return -1;
    Int idx = idx_pos.x + idx_pos.y * size + idx_pos.z * size * size;
    return idx;
}

void map_load_meshes(NetSsSgnl::ChunkMeshLoad const& net_meshes) {
    gl::VertContext::unbind_all();
    for(auto const& pair : net_meshes.meshes) {
        ChkPos chk_pos = pair.first;
        if(chunk_mesh_requests.count(chk_pos) > 0) {
            chunk_mesh_requests.erase(chk_pos);
        }
        auto const& net_mesh = pair.second;
        Int idx = get_fov_idx(chk_pos);
        if(idx < 0) {
            LUX_LOG_WARN("received mesh {%zd, %zd, %zd} out of load range",
                chk_pos.x, chk_pos.y, chk_pos.z);
            continue;
        }
        if(meshes[idx].is_allocated) {
            LUX_LOG_WARN("received mesh {%zd, %zd, %zd} already loaded",
                chk_pos.x, chk_pos.y, chk_pos.z);
            continue;
        }
        Mesh& mesh = meshes[idx];
        SizeT faces_num = 0;
        faces_num += net_mesh.faces.len;
        mesh.verts.resize(faces_num * 4);
        mesh.idxs.resize(faces_num * 6);
        Arr<Vec3<U8>, 3 * 4> vert_offs = {
            {0, 0, 0}, {0, 1, 0}, {0, 0, 1}, {0, 1, 1},
            {0, 0, 0}, {0, 0, 1}, {1, 0, 0}, {1, 0, 1},
            {0, 0, 0}, {1, 0, 0}, {0, 1, 0}, {1, 1, 0},
        };
        for(Uns i = 0; i < net_mesh.faces.len; ++i) {
            auto const& n_face = net_mesh.faces[i];
            ChkIdx idx = n_face.idx;
            U8 axis = (n_face.orientation & 0b110) >> 1;
            LUX_ASSERT(axis != 0b11);
            U8 sign = (n_face.orientation & 1);
            Vec3<U8> face_off(0);
            face_off[axis] = 1;
            if(sign) {
                for(Uns j = 0; j < 6; ++j) {
                    mesh.idxs[i * 6 + j] = quad_idxs<U16>[j] + i * 4;
                }
            } else {
                for(Uns j = 0; j < 6; ++j) {
                    mesh.idxs[i * 6 + j] = quad_idxs<U16>[5 - j] + i * 4;
                }
            }
            for(Uns j = 0; j < 4; ++j) {
                auto& vert = mesh.verts[i * 4 + j];
                vert.pos  = (Vec3<U8>)to_idx_pos(idx) + face_off +
                    vert_offs[axis * 4 + j];
                vert.norm = n_face.orientation;
                vert.tex  = n_face.id;
            }
        }
        mesh.alloc();
        mesh.v_buff.bind();
        mesh.v_buff.write(mesh.verts.len, mesh.verts.beg, GL_DYNAMIC_DRAW);
        mesh.i_buff.bind();
        mesh.i_buff.write(mesh.idxs.len, mesh.idxs.beg, GL_DYNAMIC_DRAW);
    }
}

void map_update_meshes(NetSsSgnl::ChunkMeshUpdate const& net_meshes) {
    gl::VertContext::unbind_all();
    for(auto const& pair : net_meshes.meshes) {
        ChkPos pos = pair.first;
        auto const& net_mesh = pair.second;
        Int idx = get_fov_idx(pos);
        if(idx < 0 || not meshes[idx].is_allocated) {
            LUX_LOG_WARN("received mesh update for unloaded mesh"
                " {%zd, %zd, %zd}", pos.x, pos.y, pos.z);
            continue;
        }
        Mesh& mesh = meshes[idx];
        for(auto const& removed_face : net_mesh.removed_faces) {
            for(Uns i = (removed_face + 1) * 6; i < mesh.idxs.len; ++i) {
                mesh.idxs[i] -= 4;
            }
            //@XXX this crashes sometimes
            mesh.idxs.erase(removed_face * 6, 6);
            mesh.verts.erase(removed_face * 4, 4);
        }
        Arr<Vec3<U8>, 3 * 4> vert_offs = {
            {0, 0, 0}, {0, 1, 0}, {0, 0, 1}, {0, 1, 1},
            {0, 0, 0}, {0, 0, 1}, {1, 0, 0}, {1, 0, 1},
            {0, 0, 0}, {1, 0, 0}, {0, 1, 0}, {1, 1, 0},
        };
        Uns off = mesh.verts.len / 4;
        mesh.idxs.resize(mesh.idxs.len + net_mesh.added_faces.len * 6);
        mesh.verts.resize(mesh.verts.len + net_mesh.added_faces.len * 4);
        for(Uns i = 0; i < net_mesh.added_faces.len; ++i) {
            auto const& n_face = net_mesh.added_faces[i];
            ChkIdx idx = n_face.idx;
            U8 axis = (n_face.orientation & 0b110) >> 1;
            LUX_ASSERT(axis != 0b11);
            U8 sign = (n_face.orientation & 1);
            Vec3<U8> face_off(0);
            face_off[axis] = 1;
            if(sign) {
                for(Uns j = 0; j < 6; ++j) {
                    mesh.idxs[off * 6 + j] = quad_idxs<U16>[j] + off * 4;
                }
            } else {
                for(Uns j = 0; j < 6; ++j) {
                    mesh.idxs[off * 6 + j] = quad_idxs<U16>[5 - j] + off * 4;
                }
            }
            for(Uns j = 0; j < 4; ++j) {
                auto& vert = mesh.verts[off * 4 + j];
                vert.pos  = (Vec3<U8>)to_idx_pos(idx) + face_off +
                    vert_offs[axis * 4 + j];
                vert.norm = n_face.orientation;
                vert.tex  = n_face.id;
            }
            ++off;
        }
        mesh.i_buff.bind();
        mesh.i_buff.write(mesh.idxs.len, mesh.idxs.beg, GL_DYNAMIC_DRAW);
        mesh.v_buff.bind();
        mesh.v_buff.write(mesh.verts.len, mesh.verts.beg, GL_DYNAMIC_DRAW);
    }
}

namespace map {

void render(Vec3F const& camera_pos, glm::mat4 const& tr, U32 _render_dist) {
    render_dist = _render_dist;
    ChkCoord mesh_load_size = 2 * render_dist + 1;
    ChkPos chk_pos = to_chk_pos(glm::floor(camera_pos));
    static ChkCoord last_render_dist = render_dist;
    ChkCoord last_mesh_load_size = 2 * last_render_dist + 1;

    ChkPos chk_diff           = chk_pos - last_player_chk_pos;
    ChkCoord render_dist_diff = render_dist - last_render_dist;
    chk_diff -= render_dist_diff;
    if(chk_diff != ChkPos(0)) {
        SizeT meshes_num = glm::pow(mesh_load_size, 3);
        auto get_idx = [&](ChkPos pos, ChkCoord size) {
            return pos.x +
                   pos.y * size +
                   pos.z * size * size;
        };
        static DynArr<Mesh> new_meshes;
        new_meshes.resize(meshes_num);
        for(ChkCoord z =  chk_diff.z > 0 ? chk_diff.z : 0;
                     z < last_mesh_load_size;
                   ++z) {
            for(ChkCoord y =  chk_diff.y > 0 ? chk_diff.y : 0;
                         y < last_mesh_load_size;
                       ++y) {
                for(ChkCoord x =  chk_diff.x > 0 ? chk_diff.x : 0;
                             x < last_mesh_load_size;
                           ++x) {
                    ChkPos src(x, y, z);
                    if(clamp(src, ChkCoord(0), last_mesh_load_size -
                             ChkCoord(1)) != src) {
                        continue;
                    }
                    SizeT src_idx = get_idx(src, last_mesh_load_size);
                    ChkPos dst = src - chk_diff;
                    if(clamp(dst, ChkCoord(0), mesh_load_size - ChkCoord(1)) !=
                       dst) {
                        continue;
                    }
                    SizeT dst_idx = get_idx(dst, mesh_load_size);
                    LUX_ASSERT(not new_meshes[dst_idx].is_allocated);
                    new_meshes[dst_idx] = move(meshes[src_idx]);
                }
            }
        }
        for(Uns i = 0; i < meshes.len; ++i) {
            if(meshes[i].is_allocated) {
                auto const& sz = last_mesh_load_size;
                ChkPos pos;
                pos.x = i % sz;
                pos.y = (i % (sz * sz)) / sz;
                pos.z = i / (sz * sz);
                pos = (pos - last_render_dist) + last_player_chk_pos;
                meshes[i].dealloc_and_unload(pos);
            }
        }
        swap(meshes, new_meshes);
    }

    last_player_chk_pos = chk_pos;
    last_render_dist    = render_dist;

    struct DrawData {
        U32 mesh_idx;
        Vec3F pos;
    };
    static DynArr<DrawData> draw_queue;
    draw_queue.clear();
    struct {
        U64 chunks_num = 0;
        U64 real_chunks_num = 0;
        U64 faces_num  = 0;
    } status;
    static DynArr<EntityId> entities_to_render;
    entities_to_render.clear();
    for(Uns i = 0; i < meshes.len; ++i) {
        Mesh* mesh = &meshes[i];
        ChkPos pos = { i % mesh_load_size,
                      (i / mesh_load_size) % mesh_load_size,
                       i / (mesh_load_size * mesh_load_size)};
        pos -= render_dist;
        pos += chk_pos;
        if(glm::distance((Vec3F)chk_pos, (Vec3F)pos) > render_dist) {
            continue;
        }
        Vec3F center = (Vec3F)pos * (Vec3F)CHK_SIZE + (Vec3F)(CHK_SIZE / 2);
        Vec4F c_center = tr * Vec4F(center, 1.f);
        c_center.x /= c_center.w;
        c_center.y /= c_center.w;
        F32 constexpr chk_rad = CHK_SIZE * glm::sqrt(3);
        if(c_center.z < -chk_rad) continue;
        F32 c_chk_rad = chk_rad / glm::abs(c_center.w);
        if(glm::abs(c_center.x) > 1.f + c_chk_rad ||
           glm::abs(c_center.y) > 1.f + c_chk_rad) continue;
        if(entities_chunk_map.count(pos) > 0) {
            auto const& entities = entities_chunk_map.at(pos);
            entities_to_render.reserve_at_least(entities_to_render.len +
                entities.len);
            for(auto const& entity : entities) {
                entities_to_render.emplace(entity);
            }
        }
        if(mesh->is_allocated && mesh->verts.len <= 0) continue;

        status.chunks_num++;
        draw_queue.push({i, pos});
    }
    {
        Vec3F f_pos = chk_pos;
        std::sort(draw_queue.begin(), draw_queue.end(),
            [&](DrawData const& a, DrawData const& b) {
                return glm::fastDistance(a.pos, f_pos) <
                       glm::fastDistance(b.pos, f_pos);
            });
    }
    static bool wireframe = false;
    static bool cull_faces = true;
    if(wireframe) {
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    }
    glEnable(GL_DEPTH_TEST);
    if(cull_faces) {
        glCullFace(GL_BACK);
        glEnable(GL_CULL_FACE);
    } else {
        glDisable(GL_CULL_FACE);
    }

    glUseProgram(program);
    set_uniform("tileset" , program, glUniform1i, 0);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, tileset);
    //set_uniform("time", program, glUniform1f, glfwGetTime());
    set_uniform("mvp", program, glUniformMatrix4fv,
                1, GL_FALSE, glm::value_ptr(tr));

    deferred_renderer::bind_draw_buffers();

    for(auto const& draw_data : draw_queue) {
        Mesh* mesh = &meshes[draw_data.mesh_idx];
        auto const& pos = draw_data.pos;
        if(not mesh->is_allocated) {
            //@NOTE we request the chunk here, because we want to request them
            //in a priority sorted by distance to player, i.e. closer chunks
            //are requested earlier
            chunk_mesh_requests.emplace(pos);
            continue;
        }
        if(mesh != &debug_mesh_0 && mesh != &debug_mesh_1) {
            status.real_chunks_num++;
        }
        status.faces_num += mesh->idxs.len / 6;
        Vec3F chk_translation = pos * (F32)CHK_SIZE;

        set_uniform("chk_pos", program, glUniform3fv, 1,
            glm::value_ptr(chk_translation));
        //@TODO multi draw
        mesh->context.bind();
        mesh->i_buff.bind();
        glDrawElements(GL_TRIANGLES, mesh->idxs.len, GL_UNSIGNED_SHORT, 0);
    }
    glDisable(GL_DEPTH_TEST);

    entity::render(tr, entities_to_render);

    if(wireframe) {
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    }

    deferred_renderer::unbind_draw_buffers();
    deferred_renderer::blit_depth_buffer();

    Vec3F ambient_light =
        glm::mix(Vec3F(0.01f, 0.015f, 0.02f), Vec3F(1.f, 1.f, 0.9f),
                 max(0.f, sin(tau * ss_tick.day_cycle)));

    Vec3F sun_dir = normalize(Vec3F(
        1.f,
        cos(tau * mod(ss_tick.day_cycle, 0.5f)),
        -abs(sin(tau * ss_tick.day_cycle))));
    directional_lightning::render(camera_pos, {ambient_light, sun_dir});

    static F64 avg = 0.0;
    static U32 denom = 0;
    static F64 timer = glfwGetTime();
    static F64 delta_max = 0;
    F64 now = glfwGetTime();
    F64 delta = now - timer;
    if(delta > delta_max) delta_max = delta;
    timer = now;

    SizeT constexpr plot_sz = 512;
    static F32 last_delta[plot_sz] = {0.0};
    for(Uns i = 0; i < plot_sz - 1; ++i) {
        last_delta[i] = last_delta[i + 1];
    }
    last_delta[plot_sz - 1] = (F32)(delta * 1000.0);

    avg += delta;
    denom++;
    static F64 last_avg = 0.0;
    if(denom >= 8) {
        last_avg = avg / (F64)denom;
        avg = 0.0;
        denom = 0;
    }
    F64 fps = 1.0 / last_avg;

    ImGui::Begin("render status");
    ImGui::Text("delta: %.2F", last_avg * 1000.0);
    ImGui::Text("delta_max: %.2F", delta_max * 1000.0);
    ImGui::PlotHistogram("", last_delta, plot_sz, 0,
                         nullptr, 0.f, FLT_MAX, {200, 80});
    if(ImGui::Button("wireframe mode")) {
        wireframe = !wireframe;
    }
    if(ImGui::Button("face culling")) {
        cull_faces = !cull_faces;
    }
    ImGui::Text("fps: %d", (int)fps);
    ImGui::Text("render dist: %zu", (Uns)render_dist);
    ImGui::Text("pending chunks num: %zu", chunk_mesh_requests.size());
    ImGui::Text("chunks num: %zu", status.chunks_num);
    ImGui::Text("real chunks num: %zu", status.real_chunks_num);
    ImGui::Text("faces num: %zu", status.faces_num);
    ImGui::Text("avg. faces num (real): %.2f",
        (F32)status.faces_num / (F32)status.real_chunks_num);
    ImGui::End();
}

void set_entities_chunk_map(VecMap<ChkPos, DynArr<EntityId>>&& chunk_map) {
    entities_chunk_map.clear();
    entities_chunk_map = move(chunk_map);
}

}
