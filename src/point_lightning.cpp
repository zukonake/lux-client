#include <glm/gtc/type_ptr.hpp>
//
#include <lux_shared/common.hpp>
//
#include <gl.hpp>
#include <point_lightning.hpp>
#include <deferred_renderer.hpp>

namespace point_lightning {
namespace {
    constexpr Uns sector_count = 12;
    constexpr Uns stack_count  = 8;
    constexpr Uns vert_count   = (stack_count + 1) * (sector_count + 1);
    constexpr Uns idx_count    = stack_count * sector_count * 6;

#pragma pack(push, 1)
    struct Vert {
        Vec3F pos;
    };
#pragma pack(pop)

    gl::IdxBuff     i_buff;
    gl::VertBuff    v_buff;
    gl::VertFmt     vert_fmt;
    gl::VertContext context;

    GLuint program;
}

void init() {
    Arr<Vert, vert_count> verts;

    F32 sector_step = tau / sector_count;
    F32 stack_step = (tau / 2.f) / stack_count;

    F32 stack_angle;
    F32 sector_angle;
    U32 vert_idx = 0;
    for(Uns i = 0; i <= stack_count; ++i)
    {
        stack_angle = tau / 4.f - i * stack_step;
        F32 xy = cosf(stack_angle);
        F32 z = sinf(stack_angle);

        for(Uns j = 0; j <= sector_count; ++j)
        {
            sector_angle = j * sector_step;

            F32 x = xy * cosf(sector_angle);
            F32 y = xy * sinf(sector_angle);

            verts[vert_idx++] = {{x, y, z}};
        }
    }
    Arr<U32, idx_count> idxs;

    int k1, k2;
    Uns idxi = 0;
    for(int i = 0; i < stack_count; ++i)
    {
        k1 = i * (sector_count + 1);     // beginning of current stack
        k2 = k1 + sector_count + 1;      // beginning of next stack

        for(int j = 0; j < sector_count; ++j, ++k1, ++k2)
        {
            // 2 triangles per sector excluding first and last stacks
            // k1 => k2 => k1+1
            if(i != 0)
            {
                idxs[idxi++] = k1;
                idxs[idxi++] = k2;
                idxs[idxi++] = k1 + 1;
            }

            // k1+1 => k2 => k2+1
            if(i != (stack_count-1))
            {
                idxs[idxi++] = k1 + 1;
                idxs[idxi++] = k2;
                idxs[idxi++] = k2 + 1;
            }
        }
    }

    gl::VertContext::unbind_all();
    v_buff.init();
    v_buff.bind();
    v_buff.write(vert_count, verts, GL_STATIC_DRAW);

    i_buff.init();
    i_buff.bind();
    i_buff.write(idx_count, idxs, GL_STATIC_DRAW);

    program = load_program("glsl/point_lightning.vert",
                           "glsl/point_lightning.frag");
    vert_fmt.init({{3, GL_FLOAT, false, false}}),
    context.init({v_buff}, vert_fmt);
}

void deinit() {
    context.deinit();
    i_buff.deinit();
    v_buff.deinit();
}

void render(Vec3F const& camera_pos, glm::mat4 const& transform,
            Slice<Source> sources) {
    glEnable(GL_BLEND);
    glBlendFunc(GL_ONE, GL_ONE);
    glEnable(GL_CULL_FACE);
    glCullFace(GL_FRONT);

    glUseProgram(program);

    deferred_renderer::bind_buffer_textures(0);

    set_uniform("g_buffs.pos" , program, glUniform1i, 0);
    set_uniform("g_buffs.norm", program, glUniform1i, 1);
    set_uniform("g_buffs.col" , program, glUniform1i, 2);
    set_uniform("transform", program, glUniformMatrix4fv,
                1, GL_FALSE, glm::value_ptr(transform));
    set_uniform("camera_pos", program, glUniform3fv,
                1, glm::value_ptr(camera_pos));

    context.bind();
    i_buff.bind();

    for(auto const& source : sources) {
        set_uniform("light.pos", program, glUniform3fv,
                    1, glm::value_ptr(source.pos));
        set_uniform("light.col", program, glUniform3fv,
                    1, glm::value_ptr(source.col));
        set_uniform("light.rad", program, glUniform1f, source.rad);
        glDrawElements(GL_TRIANGLES, idx_count, GL_UNSIGNED_INT, 0);
    }

    glDisable(GL_BLEND);
    glCullFace(GL_BACK);
    glDisable(GL_CULL_FACE);
}

}
