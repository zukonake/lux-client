#include <cstdint>
#include <cstring>
//
#include <glm/gtc/type_ptr.hpp>
//
#include <lux_shared/common.hpp>
//
#include <gl.hpp>
#include <ui.hpp>
#include <client.hpp>
#include <imgui/imgui.h>
#include <imgui/imgui_impl_opengl3.h>
#include <imgui/imgui_impl_glfw.h>

namespace ui {

Handle screen;
static IoContext io_context;

static SparseDynArr<Node, NodeId> nodes;

bool Handle::is_valid() const {
    return nodes.contains(id);
}

Node& Handle::operator*() const {
    return nodes[id];
}

Node* Handle::operator->() const {
    return nodes.at(id);
}

static Handle imgui;
static void imgui_tick(Handle, Transform const&, IoContext&);

void default_io_tick(Handle h, Transform const& tr, IoContext& c) {
    children_tick(h, tr * h->tr, c);
}

Handle create_node(Handle parent, U8 priority) {
    NodeId id = nodes.emplace();
    LUX_LOG_DBG("creating UI #%u", id);
    //@TODO priority might be deprecated along with the addition of Z-levels
    nodes[id].priority = priority;
    LUX_ASSERT(parent.is_valid());
    parent->children.emplace(Handle{id});
    std::sort(parent->children.begin(),
              parent->children.end(),
              [](auto const& a, auto const& b) {
                  return a.is_valid() && b.is_valid() &&
                         a->priority < b->priority;
              });
    return {id};
}

void Handle::erase() {
    LUX_LOG_DBG("erasing UI #%u", id);
    LUX_ASSERT(is_valid());
    auto& node = *(*this);
    for(auto& child : node.children) {
        child.erase();
    }
    if(node.deinit != nullptr) {
        (node.deinit)(0); //@TODO arg
    }
    nodes.erase(id);
}

void window_sz_cb(Vec2U const& old_sz, Vec2U const& sz) {
    LUX_UNIMPLEMENTED();
}

void init() {
    LUX_ASSERT(nodes.size() == 0);
    nodes.emplace();
    screen->tr     = glm::mat4(1.f);
    imgui          = create_node(screen, 0x0);
    imgui->io_tick = &imgui_tick;

    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGui_ImplGlfw_InitForOpenGL(glfw_window, false);
    ImGui_ImplOpenGL3_Init(nullptr);
    ImGui::StyleColorsDark();
}

void deinit() {
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();
    Handle{screen.id}.erase();
    LUX_ASSERT(nodes.size() == 0);
}

static void imgui_tick(Handle, Transform const&, IoContext& c) {
    static bool cursor_disabled = true;
    static Vec2F last_mouse_pos = Vec2F(0.f, 0.f);
    for(auto const& ev : c.key_events) {
        if(ev.action == GLFW_PRESS && ev.key == GLFW_KEY_TAB) {
            cursor_disabled = !cursor_disabled;
            if(cursor_disabled) {
                glfwSetInputMode(c.win, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
            } else {
                glfwSetInputMode(c.win, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
                last_mouse_pos = c.mouse_pos;
            }
        }
    }

    if(not cursor_disabled) {
        for(auto const& ev : c.key_events) {
            const auto sc = glfwGetKeyScancode(ev.key);
            ImGui_ImplGlfw_KeyCallback(c.win, ev.key, sc, ev.action, 0);
        }
        c.key_events.clear();
        for(auto const& ev : c.scroll_events) {
            ImGui_ImplGlfw_ScrollCallback(c.win, 0, ev.off);
        }
        c.scroll_events.clear();
        for(auto const& ev : c.mouse_events) {
            ImGui_ImplGlfw_MouseButtonCallback(c.win, ev.button, ev.action, 0);
        }
        c.mouse_events.clear();
        c.mouse_pos = last_mouse_pos;
    }
    static auto previous_player_pos = EntityVec(0.f);
    {   ImGui::Begin("debug info");
        ImGui::Text("pos: {%.2f, %.2f, %.2f}",
            last_player_pos.x, last_player_pos.y, last_player_pos.z);
        ChkPos chk_pos = to_chk_pos(floor(last_player_pos));
        ImGui::Text("chk_pos: {%zd, %zd, %zd}",
            chk_pos.x, chk_pos.y, chk_pos.z);
        IdxPos idx_pos = to_idx_pos(floor(last_player_pos));
        ImGui::Text("idx_pos: {%u, %u, %u}",
            idx_pos.x, idx_pos.y, idx_pos.z);
        ChkIdx chk_idx = to_chk_idx(idx_pos);
        ImGui::Text("chk_idx: {%u}", chk_idx);
        F32 vel = length(last_player_pos - previous_player_pos);
        ImGui::Text("vel: {%.2f}", vel);
        ImGui::End();
        previous_player_pos = last_player_pos;
    }
}

void send_mouse_event(MouseEvent const& event) {
    io_context.mouse_events.emplace(event);
}

void send_scroll_event(ScrollEvent const& event) {
    io_context.scroll_events.emplace(event);
}

void send_key_event(KeyEvent const& event) {
    io_context.key_events.emplace(event);
}

static void tick(Handle handle, Transform const& tr, IoContext& c) {
    Transform new_tr = handle->tr * tr;
    if(handle->io_tick != nullptr) {
        //@TODO mouse needs a negative translation in transform??
        //@TODO mouse gets untransformed coords btw...
        handle->io_tick(handle, new_tr, c);
    }
}

void children_tick(Handle h, Transform const& tr, IoContext& c) {
    for(auto const& child : h->children) {
        tick(child, tr, c);
    }
}

void tick() {
    Vec2<F64> mouse_pos;
    glfwGetCursorPos(glfw_window, &mouse_pos.x, &mouse_pos.y);
    io_context.mouse_pos = (Vec2F)mouse_pos;
    io_context.win = glfw_window;
    tick(screen, glm::mat4(1.f), io_context);
    nodes.free_slots();
    io_context.mouse_events.clear();
    io_context.scroll_events.clear();
    io_context.key_events.clear();
}

}
