#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/quaternion.hpp>
#include <imgui/imgui.h>
//
#include <db.hpp>
#include <gl.hpp>
#include <client.hpp>
#include <ui.hpp>
#include <map.hpp>
#include <deferred_renderer.hpp>
#include "entity.hpp"

static ui::Handle entities_ui;
static ui::Handle camera_ui;
static ui::Handle map_ui;
static EntityComps comps;
EntityComps& entity_comps = comps;
DynArr<EntityId> entities;

static GLuint program;

#pragma pack(push, 1)
struct Vert {
    Vec3F pos;
    Vec3F norm;
};
#pragma pack(pop)

static gl::VertBuff    v_buff;
static gl::IdxBuff     i_buff;
static gl::VertContext context;
static gl::VertFmt     vert_fmt;

static void entity_io_tick(ui::Handle, ui::Transform const&, ui::IoContext&);

void entity_init() {
    program = load_program("glsl/entity.vert", "glsl/entity.frag");

    vert_fmt.init(
        {{3, GL_FLOAT, false, false},
         {3, GL_FLOAT, false, false}});

    glUseProgram(program);

    v_buff.init();
    i_buff.init();
    context.init({v_buff}, vert_fmt);

    Arr<Vert, 24> verts = {
        {{-0.8f,  0.8f, -1.8f}, {-1.f,  0.f,  0.f}},
        {{-0.8f, -0.8f, -1.8f}, {-1.f,  0.f,  0.f}},
        {{-0.8f,  0.8f,  1.8f}, {-1.f,  0.f,  0.f}},
        {{-0.8f, -0.8f,  1.8f}, {-1.f,  0.f,  0.f}},

        {{ 0.8f, -0.8f,  1.8f}, { 1.f,  0.f,  0.f}},
        {{ 0.8f, -0.8f, -1.8f}, { 1.f,  0.f,  0.f}},
        {{ 0.8f,  0.8f,  1.8f}, { 1.f,  0.f,  0.f}},
        {{ 0.8f,  0.8f, -1.8f}, { 1.f,  0.f,  0.f}},

        {{ 0.8f, -0.8f, -1.8f}, { 0.f, -1.f,  0.f}},
        {{ 0.8f, -0.8f,  1.8f}, { 0.f, -1.f,  0.f}},
        {{-0.8f, -0.8f, -1.8f}, { 0.f, -1.f,  0.f}},
        {{-0.8f, -0.8f,  1.8f}, { 0.f, -1.f,  0.f}},

        {{-0.8f,  0.8f, -1.8f}, { 0.f,  1.f,  0.f}},
        {{-0.8f,  0.8f,  1.8f}, { 0.f,  1.f,  0.f}},
        {{ 0.8f,  0.8f, -1.8f}, { 0.f,  1.f,  0.f}},
        {{ 0.8f,  0.8f,  1.8f}, { 0.f,  1.f,  0.f}},

        {{ 0.8f, -0.8f,  1.8f}, { 0.f,  0.f,  1.f}},
        {{ 0.8f,  0.8f,  1.8f}, { 0.f,  0.f,  1.f}},
        {{-0.8f, -0.8f,  1.8f}, { 0.f,  0.f,  1.f}},
        {{-0.8f,  0.8f,  1.8f}, { 0.f,  0.f,  1.f}},

        {{ 0.8f, -0.8f, -1.8f}, { 0.f,  0.f, -1.f}},
        {{-0.8f, -0.8f, -1.8f}, { 0.f,  0.f, -1.f}},
        {{ 0.8f,  0.8f, -1.8f}, { 0.f,  0.f, -1.f}},
        {{-0.8f,  0.8f, -1.8f}, { 0.f,  0.f, -1.f}},
    };
    Arr<U32, 36> idxs;
    for(Uns s = 0; s < 6; ++s) {
        for(Uns i = 0; i < 6; ++i) {
            idxs[s * 6 + i] = quad_idxs<U32>[i] + s * 4;
        }
    }
    v_buff.bind();
    v_buff.write(24, verts, GL_STATIC_DRAW);
    i_buff.bind();
    i_buff.write(36, idxs, GL_STATIC_DRAW);
}

void set_net_entities(NetSsTick::Entities&& net_comps) {
    entities.resize(net_comps.pos.size());
    comps.pos.clear();
    comps.name.clear();
    comps.model.clear();
    VecMap<ChkPos, DynArr<EntityId>> chunk_map;
    for(auto const& pos : net_comps.pos) {
        entities.emplace(pos.first);
        comps.pos[pos.first] = pos.second;
        chunk_map[to_chk_pos(floor(pos.second))].emplace(pos.first);
    }
    map::set_entities_chunk_map(move(chunk_map));
    for(auto const& name : net_comps.name) {
        comps.name[name.first].resize(arr_len(name.second));
        memcpy(comps.name[name.first].beg, name.second, sizeof(name.second));
    }
    comps.health = move(net_comps.health);
    comps.inventory = move(net_comps.inventory);
    comps.voxel_item = move(net_comps.voxel_item);
    comps.stack = move(net_comps.stack);
}

namespace entity {

Vec3F get_player_pos() {
    if(comps.pos.count(player_id) > 0) {
        return comps.pos.at(player_id);
    } else {
        return Vec3F(0.f, 0.f, 0.f); //@TODO return false function failed
    }
}

void render(glm::mat4 const& transform, Slice<EntityId> queue) {
    glUseProgram(program);

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    for(auto const& entity : queue) {
        if(entity == player_id) {
            if(comps.inventory.count(entity) > 0) {
                ImGui::Begin("inventory");
                U32 id = 0;
                for(auto const& item : comps.inventory.at(entity).items) {
                    ImGui::PushID(id);
                    if(comps.name.count(item) > 0) {
                        auto const& name = comps.name.at(item);
                        ImGui::Text("%.*s", (int)name.len, name.beg);
                    } else if(comps.voxel_item.count(item) > 0) {
                        auto const& bp = db_block_bp(comps.voxel_item.at(item));
                        ImGui::Text("%.*s", (int)bp.name.len, bp.name.beg);
                    } else {
                        ImGui::Text("unknown item");
                    }
                    F32 q = 1.f;
                    if(comps.stack.count(item) > 0) {
                        q = comps.stack.at(item);
                    }
                    ImGui::SameLine();
                    ImGui::Text("x%.2f", q);
                    if(ImGui::BeginPopupContextItem("item menu")) {
                        if(ImGui::Selectable("eat")) {
                            cs_sgnl.tag = NetCsSgnl::DO_INTERACTION;
                            cs_sgnl.do_interaction = {
                                NetCsSgnl::DoInteraction::EAT, item};
                            client_send_signal();
                        }
                        ImGui::EndPopup();
                    }
                    ImGui::PopID();
                    id++;
                }
                ImGui::End();
            }
            if(comps.health.count(entity) > 0) {
                ImGui::Begin("health");
                ImGui::Columns(2);
                for(auto const& stat : comps.health.at(entity).stats) {
                    ImGui::Text("%.*s: %.2f",
                        (int)stat.name.len, stat.name.beg, stat.val);
                    ImGui::NextColumn();
                    ImGui::ProgressBar(stat.val);
                    ImGui::NextColumn();
                }
                ImGui::End();
            }
        }
        if(entity != player_id && comps.pos.count(entity) > 0) {
            static Uns i = 0;
            Vec3F pos = comps.pos.at(entity);
            glm::mat4 model = glm::translate(glm::mat4(1.f), pos);

            set_uniform("transform", program, glUniformMatrix4fv,
                        1, GL_FALSE, glm::value_ptr(transform * model));
            set_uniform("map_pos", program, glUniform3fv,
                        1, glm::value_ptr(pos));

            context.bind();
            v_buff.bind();
            i_buff.bind();

            glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, 0);
        }
    }
    glDisable(GL_CULL_FACE);
    glDisable(GL_DEPTH_TEST);
}

}
