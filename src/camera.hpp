#pragma once

namespace camera {

void init();
void render(Vec3F player_pos);

}
