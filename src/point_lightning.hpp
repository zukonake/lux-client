#pragma once

#include <lux_shared/common.hpp>

namespace point_lightning {

void init();
void deinit();

struct Source {
    Vec3F pos;
    Vec3F col;
    F32   rad;
};

void render(Vec3F const& camera_pos, glm::mat4 const& transform,
            Slice<Source> sources);

}
