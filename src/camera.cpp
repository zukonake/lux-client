#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>
//
#include <lux_shared/common.hpp>
//
#include <ui.hpp>
#include <map.hpp>
#include <client.hpp>
#include <deferred_renderer.hpp>
#include <camera.hpp>

namespace camera {

namespace {
    U32 render_dist = 0;

    glm::mat4 projection_mat(1.f);
    glm::mat4 view_mat(1.f);

    Vec3F camera_pos(0.f);

    ui::Handle ui_h;

    void input_tick(ui::Handle, ui::Transform const&, ui::IoContext& context);
}

void init() {
    ui_h = ui::create_node(ui::screen, 0xff);
    ui_h->io_tick = &input_tick;
}

void render(Vec3F player_pos) {
    //@TODO probably doesn't belong to dr and shouldnt be called here
    deferred_renderer::clear_screen();
    camera_pos = player_pos + Vec3F(0, 0, 1.5f); //@TODO height
    glm::mat4 mvp = projection_mat * view_mat;
    map::render(player_pos, mvp, render_dist);
    deferred_renderer::blit_depth_buffer();
}

namespace {

void input_tick(ui::Handle, ui::Transform const&, ui::IoContext& context) {
    //@TODO better event handling...
    for(Uns i = 0; i < context.mouse_events.len; ++i) {
        //@TODO erase event
        auto const& event = context.mouse_events[i];
        if(event.action == GLFW_PRESS) {
            if(event.button == GLFW_MOUSE_BUTTON_LEFT) {
                //@TODO client::place_block etc.
                cs_sgnl.tag       = NetCsSgnl::DO_ACTION;
                cs_sgnl.do_action = NetCsSgnl::DoAction::BREAK_BLOCK;
                (void)client_send_signal();
            } else if(event.button == GLFW_MOUSE_BUTTON_RIGHT) {
                cs_sgnl.tag       = NetCsSgnl::DO_ACTION;
                cs_sgnl.do_action = NetCsSgnl::DoAction::PLACE_BLOCK;
                (void)client_send_signal();
            }
        }
    }

    F32 render_dist_change = 0.f;
    for(Uns i = 0; i < context.scroll_events.len; ++i) {
        //@TODO erase event
        auto const& event = context.scroll_events[i];
        render_dist_change += event.off;
    }
    render_dist = round(max((F32)render_dist + render_dist_change, 0.f));
    //@TODO glfwGetKey?
    Vec3F dir(0.f);
    if(glfwGetKey(context.win, GLFW_KEY_W)) {
        dir.x += 1.f;
    }
    if(glfwGetKey(context.win, GLFW_KEY_S)) {
        dir.x -= 1.f;
    }
    if(glfwGetKey(context.win, GLFW_KEY_D)) {
        dir.y -= 1.f;
    }
    if(glfwGetKey(context.win, GLFW_KEY_A)) {
        dir.y += 1.f;
    }
    if(glfwGetKey(context.win, GLFW_KEY_SPACE)) {
        cs_tick.is_jumping = true;
    } else {
        cs_tick.is_jumping = false;
    }
    F32 len = length(dir);
    if(len != 0.f) {
        dir /= len;
        cs_tick.is_moving = true;
        cs_tick.move_dir = dir;
    } else {
        cs_tick.is_moving = false;
    }

    //@TODO
    static Vec2F last_mouse_pos = {0, 0};
    static F32 yaw   = 0.f;
    static F32 pitch = 0.f;
    Vec2F diff = (last_mouse_pos - context.mouse_pos) * 0.005f;
    last_mouse_pos = context.mouse_pos;

    diff.y = -diff.y;

    yaw   += diff.x;
    pitch += diff.y;
    yaw    = s_mod_clamp(yaw, tau / 2.f);
    pitch  = clamp(pitch, -tau / 4.f + .001f, tau / 4.f - .001f);
    cs_tick.yaw_pitch = {yaw, pitch};

    Vec3F direction(1.f, 0.f, 0.f);
    glm::quat quat = glm::angleAxis(yaw  , Vec3F{0.f, 0.f, 1.f}) *
                     glm::angleAxis(pitch, Vec3F{0.f, 1.f, 0.f});
    direction = glm::rotate(quat, direction);

    F32 z_far = ((F32)render_dist + 1.f) * (F32)CHK_SIZE;

    int win_w, win_h;
    //@TODO we probably should use something more universal, like our local
    //ui size
    glfwGetWindowSize(context.win, &win_w, &win_h);
    F32 aspect_ratio = (F32)win_w / (F32)win_h;
    projection_mat =
        glm::perspective(glm::radians(90.f), aspect_ratio, 0.1f, z_far);

    view_mat = glm::lookAt(camera_pos, camera_pos + direction, Vec3F(0, 0, 1));
}

}

}
