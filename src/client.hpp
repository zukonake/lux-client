#pragma once

#include <lux_shared/common.hpp>
#include <lux_shared/entity.hpp>
#include <lux_shared/net/data.hpp>

//@TODO remove those
//in the case of last_player_pos it shouldnt be needed when entity tells map to
//render and passes its position
extern EntityVec last_player_pos;
extern F64 tick_rate;
extern EntityId player_id;

extern NetCsTick cs_tick;
extern NetCsSgnl cs_sgnl;
extern NetSsTick ss_tick;
extern NetSsSgnl ss_sgnl;

//@TODO not sure whether this is how we should handle signals, maybe a queue
//instead?
LUX_MAY_FAIL client_send_signal();
LUX_MAY_FAIL client_init(char const* server_hostname, U16 server_port);
void client_deinit();
LUX_MAY_FAIL client_tick(GLFWwindow* glfw_window);
void client_quit();
bool client_should_close();
